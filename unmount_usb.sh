#!/bin/bash
#
# Unmount a usb-connected partition that is mounted at /run/...
#
version="version 1.01"
label="${1-}"
[[ "$label" == '--help' || "$label" = '-h' ]] && {
	echo -e "\nRun:\n\t${0##*/} fs_label";
	echo -e "\nAn initial part of the file-system label is enough, too."
	exit;
}
[[ -z "$label" ]] && { echo Missing partition label; exit 1;}
echo -e "\nLooking for a mounted /run/media/${USER}/${label}[...]"
dev="$(lsblk -pnlo name,mountpoint |sed -n '/'"\/run\/media\/${USER}\/${label}"'/s/ \+.*//p')"
[[ -z "$dev" ]] || echo dev=$dev
(($(cat <<<$dev|wc -l) > 1)) && {
	echo More than one partition found.;
	echo REFINE FILETER \(Use unambiguous partition label\);
	exit 2;
}
[[ -z "$dev" ]] && {
	echo No mounted USB-partitions labeled \'$label\' have been found.;
	exit 3;
} || udisksctl unmount -b "$dev"
