#!/bin/bash
#
# Mount a usb-connected partition to /run/media/$USER/...
#
version="version 0.99"

not_mounted_USB_partitions(){
	for f in $(find /dev/disk/*/ -name '*usb*' -exec readlink {} \;|sort -u|grep -E '[0-9]$' | sed 's/..\/../\/dev/' )
	do
		mount -l| grep -q $f || echo $f;
	done
}

label="${1-}"
[[ "$label" == '--help' || "$label" = '-h' ]] && {
	echo -e "\nRun:\n\t${0##*/} fs_label";
	echo -e "\nAny part of the file-system label is enough, too."
	echo If only one USB partition is available, it will be mounted, no questions asked.
	exit;
}
[[ "$label" == '-d' ]] && { debug=1; label="${2-}";}

USB_parts="$(not_mounted_USB_partitions)"
# Only partitions with a fs label; drop others.
((debug)) && echo -e "DEBUG.1\nNot mounted USB partitions:\n$USB_parts"

if [[ -z "$USB_parts" ]]
then
	USB_fs_labels=''
else
	USB_fs_labels=$(lsblk -pnlo name,label $USB_parts | awk 'NF>1 {print $0}')
fi
((debug)) && echo -e "DEBUG.2\nAvailable USB partitions:\n$USB_fs_labels"

[[ -z "$label" ]] || USB_fs_labels=$(grep "$label" <<<"$USB_fs_labels")
((debug)) && echo -e "DEBUG.3\nUSB partitions filtered with LABEL:\n$USB_fs_labels"

N=$(wc -l <<<"$USB_fs_labels")
((debug)) && echo DEBUG.4 N=$N
[[ -z "$USB_fs_labels" ]] && ((--N)) # Correction (because wc -l <<<"" == 1)
((debug)) && echo Number of available partitions, N=$N

echo
case $N in
	1) udisksctl mount -b "${USB_fs_labels%% *}";;
	0) echo No available USB partitions found.;;
	*) echo Available USB\'s and their labels;
	   echo -e "Provide an unambiguous FS LABEL\n";
	   sed 's/^/-->\t/' <<<"$USB_fs_labels";;
esac;
