#!/bin/bash
version=0.02
declare -i CLEAN=0  # Do not remove original files.
declare -i RMEXT=0  # Keep file extension and add .7z to it. -> .ext.7z
declare -i ret_val  # return value of 7z

(($#)) || {
	echo \$\#=$#
	echo Missing input files.;
	exit 1;
}

if [[ "$1" == '-h' || "$1" == '--help' ]]
then
	echo -e "\n${0##*/} [-c][-r] files_to_be_7zipped\n"
	echo "-c  :  remove original files if compression is successful"
	echo "-r  :  remove original extension for the newly created 7z file name."
	exit
fi

if [[ "$1" == '-v' || "$1" == '--version' ]]
then
	echo -e "\n${0##*/} v.${version}"
	exit
fi

while getopts "cr" flag
do
 case "$flag" in
  c) CLEAN=1;;    # Remove original files, leaving 7z's only.
  r) RMEXT=1;;    # Remove original extension from the new name -> .ext.7z
 esac
done
# Remove the options parsed above.
shift `expr $OPTIND - 1`

(($#)) || {
	echo Missing input files.;
	exit 1;
}

for f in "$@"
do
	if [[ -r "$f" ]]
	then
		((RMEXT)) && ofile="${f%.*}.7z" || ofile="${f}.7z"
		if [[ "${f##*.}" != '7z' ]]
		then
			7z a -t7z "$ofile" "$f" >/dev/null && {
				chmod --reference="$f" "$ofile";
				((CLEAN)) && rm "$f";
			}
		else
			echo "Skipped:  $f"
		fi
	else
		echo "File '$f' : Not readable or non-existent."
	fi
done
:
