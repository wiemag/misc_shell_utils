#!/usr/bin/env bash
# https://askubuntu.com/questions/8452/font-viewer-for-font-collectors
# by https://askubuntu.com/users/794616/nharward
# touched by https://askubuntu.com/users/921072/peter7775
# touched by wm


usage_info() {
	echo -e "\nRun:\n\t${0##*/} [-h|--help]\n"
	cat INFO
	Compiles a list of installed fonts into a temporary html file,
	and displays it in a default browser.
}

if [[ "${1:-}" == '--help' || "$1" == '-h' ]]
then
	usage_info
	exit 0
fi

browsers=(firefox chromium dillo eolie epiphany falkon fiery konqueror midori netsurf nyxt opera palemoon vivaldi anglefish brave waterfox slimjet tor seamonkey librewolf)

HTMLFILE="$(mktemp --suffix=.html)" && \
cat > "$HTMLFILE" << __HEADER
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Sample of local fonts matching '$1'</title>
</head>
<body>
__HEADER

fc-list --format='%{family}\n' $1 | sort -u | while IFS='' read -r fontfamily
do
    cat >> "$HTMLFILE" << __BODY
    <hr/>
    <div style="font-family: '${fontfamily}', 'serif'">
        <h1>${fontfamily}</h1>
        <p style="font-size: 24px">
            The quick brown fox jumped over the lazy brown dog.
            <br/>0123456789&emsp;,.:;?/<>'"[]{}|\-=\`~!@#$%^&*()-=\\
            <br/>Ąą Ćć Ęę Łł Ńń Óó Śś Żż Źź π Ω ß€ ÄäÅå Ññ ⌀ ∅ ø •
        </p>
    </div>
__BODY

done

cat >> "$HTMLFILE" << __FOOTER
    <hr/>
</body>
</html>
__FOOTER

echo "'$HTMLFILE' created"
if hash xdg-open 2>/dev/null
then
	xdg-open "$HTMLFILE"
elif hash firefox 2>/dev/null
then
	xfirefox "$HTMLFILE" &
elif hash chromium 2>/dev/null
then
	xchromium "$HTMLFILE" &
fi
sleep 1
rm "$HTMLFILE"
