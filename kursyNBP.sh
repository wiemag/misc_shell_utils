#!/bin/bash
# Download currency exchange-rate tables for the NBP (the Polish Central Bank)
VERSION=2.00

# List of available tables (Lines end in <CR><LF>).
# <LF> is a separator here; <CR> needs to be removed later in the script.
URL="http://www.nbp.pl/kursy/xml/dir.txt"	# Current-year list of tables
URL2022="${URL%.txt}2022.txt" # year 2022
URL2021="${URL%.txt}2021.txt" # year 2021, etc.
table_date_fmt='a[0-9]\{3\}z[0-9]\{6\}' # a<000-number>z<RRMMDD-date>

URLA="http://www.nbp.pl/kursy/xml/dir.aspx?tt=A"
# ---------------------------------------------
# API NBP offers tabels with xlm tags in English.
# They can't be used in old software, like wysyłki.exe
# ------------
# API="https://api.nbp.pl/"
# API+="api/exchangerates/tables/A/"
# ---------------------------------------------

function WARNING_YEAR(){
	echo -e "\e[0;31m"
	echo Due to the NBP new API for exchange rates, NBP may not
	echo continue to make the old-format xml tables public.
	echo If NBP continues to publish the old format, the adress for
	echo year-2023 table is likely to be different in year 2024.
	echo This will definitely, require re-coding this script.
	echo -e "\e[0m"
}

function date_validity(){
local D="$1" # format assumed/verified is RRYYDD
	[[ -n $D ]] && [[ "${D: -6}" == $(date -d ${D: -6} +%y%m%d 2>/dev/null) ]]
}

CDATE="$(date +%y%m%d)"
[[ $# == 0 ]] && DATE="z${CDATE}" || DATE=""
TABLE="" 	# An array of table-name files.
CYEAR=$(date +%y) 	# Current year
SINCE=0		# a since-mode (since a this year's data till now)

function show_help {
 echo -e "\nDownload the chosen NBP currency exchange-rate table(s)."
 echo -e "\nUsage:\n\t${0##*/} [-d DATE] | [-s DATE] | [-h|?]\n"
 echo "DATE format:  '[RR]RRMMDD'"
 echo -e "\n'-d'   the exchange-rate table for a given DATE."
 echo "       The default and prioritised option."
 echo -e "'-s'   the exchange-rate table since a given DATE."
 echo "       Works only for the current year."
 echo -e "\n\e[0;32mDefault:\e[1m '-d <current date>'\e[0m"
 exit
}

function available_tabs(){
local D="$1"
	echo -e "$(curl -s "$URL" | sed -n '/a...z'"$DATE"'/,${/^a/p}'|cut -c 1-11)"
}

[[ "$1" = "--help" ]] && show_help

# A POSIX variable
#OPTIND=1         # Reset in case getopts has been used previously in the shell.
while getopts "d:s:h?v" opt; do
    case "$opt" in
    h|\?) show_help; exit 0;;
    d)  DATE="00000$OPTARG"; break;;
    s)  DATE="00000$OPTARG"; SINCE=1; break;;
    v)  echo ${0##*/}, version ${VERSION}; exit;;
    esac
done
shift $((OPTIND-1))
[[ -z "$DATE" ]] && DATE=${1:-} # if user forgot the -d
[[ -z "$DATE" ]] && { echo Wrong parameters\!; exit 4;}

DATE="${DATE//-/}" # remove dashes if put there absent-mindedly
DATE="${DATE:${#DATE}-6}"

date_validity "$DATE" || { echo -e "\nThe input date is invalid"; exit 5;}

((DATE > CDATE)) && {
	echo "Wrong date ($DATE).";
	echo The date cannot be greater than $CDATE;
	exit 3;
}

YEAR="${DATE:0:2}"
(( CDATE > 231221 )) && WARNING_YEAR
# DEBUG
#echo Debug: DATE=$DATE CDATE=$CDATE
#echo Debug: YEAR=$YEAR CYEAR=$CYEAR

if [[ "$SINCE" -eq 1 && "$YEAR" -ne "$CYEAR" ]]
then
	echo The '-s' \(since\) option can only be used for the current year.
	exit
fi

# The lines in the file with table names end with a <CR>
# That <CR> must be removed in a way (e.g. grep -o, or cut -c 1-11).
# Reminder: $DATE may include the leading "z" -- check it writing the code.
# Find "A" exchange-rate tables only.
if [[ "$SINCE" -eq 0 ]]; then
	(( YEAR < CYEAR )) && URL="${URL%.txt}20${YEAR}.txt"
	TABLE="$(curl -s "$URL" | grep -Eo "a...z${DATE}")"
else
	# if the first date is a free day, find the next date with the table
	while ((DATE <= CDATE))
	do
		TABLE="$(available_tabs "$DATE")"
		[[ -z "$TABLE" ]] && ((++DATE)) || break
	done
fi
[[ -z "$TABLE" ]] && { echo No table for this date.; exit;}

while read tab; do
	tab=${tab}.xml
	#echo ${URL%/*}/${tab}
	curl -s ${URL%/*}/${tab} -o ${tab}
	[[ $? == 0 ]] && echo ${tab} has been downloaded.
done <<<"$TABLE"
