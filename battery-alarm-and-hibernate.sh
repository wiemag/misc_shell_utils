#!/bin/bash
# DEPENDENCIES:
# awk, battery.sh (based on upower), libnotify, systemd, bc

hibernate_at_level=6 # 6 per cent

while :
do
	x=$(acpi) 	# Alternatywnie upower -i $(upower -d|grep BAT)

	# Exit if on AC again
	grep -q Discharging <<<"$x" || {
		batx="$(battery.sh|sed -e 's/  \+/   /g' -e 's/hours/hrs/')" 
		notify-send -t 10000 -u critical "Battery:" "$batx" && \
			unset batx
		exit 0
	}

	# Still on battery
	current_battery_level=$(grep -Po '[0-9]+(?=%)' <<<"$x")
	time_remaining=$(awk '{print $5}' <<<"$x" |awk -F: '{ print $1*3600 + $2*60 + $3 }')
	# skorygowany
	time_remaining=$(bc <<< "scale=2; $time_remaining * ($current_battery_level - $hibernate_at_level)  / $current_battery_level / 60")' minutes'
	trem=${time_remaining%.*}
	#echo DEBUG: $trem
	if (( trem > 60 ))
	then
		time_remaining=$(bc <<< "scale=2; ${time_remaining% *} / 60")' hours'
	fi
	notify-send -t 4000 Warning "Hibernating\n\tin $time_remaining" --icon=dialog-information
	#[[ $trem -gt 2 ]] && sleep $((60*(trem-1)+60)) || sleep 30
	sleep 12
	[[ $current_battery_level -le $hibernate_at_level ]] && \
		{
			systemctl -i hybrid-sleep
			exit 16
		}
done
