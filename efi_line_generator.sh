#!/bin/bash
# Creates a .nsh file in the efi/boot partition of the usb drive
# the scrip it run fron.
# Can add an efi boot entry to the EFI on the computer it is run from.
VERSION=0.10 # 2023-07-11 by wm

l_disk=''	# loader disk
l_part=''	# loader-disk partition
loader=''	# Normally, it is vmlinuz-linux
ucode=''	# if installed by pacman, else ucode=''
r_disk=''	# root disk PARTLABEL
eLabel=''	# EFI entry label
extra_opts='' # extra kernel boot options
fallbk=0 	# Use fallback initramfs image?
quiet='quiet'    # quiet boot mode
verb='--verbous' # be verbous about the efibootmgr command
Profile=0 	 # create a profil
NSH_script=0 # create a .nsh script
AddEntry=0   # add EFI boot entry

function are_you_sure(){
  echo
  read -rsn1 -p  "Complete the action? [y/N]  "
  echo
  [[ "${REPLY,,}" =~ ^(yes|y) ]] && return 0 || return 1
}

function abortf(){
	[ $(type -t "$1")"" == 'function' ] && "$1" || echo -e "$1"
    exit "${2-0}"
}

# Used if -d flag not used.
function guess_loader_disk(){
local d sh_path mounted_disks
	sh_path=${0%/*} 	#
	[[ "$sh_path" = "${0}" ]] && sh_path=''
	[[ -z "$sh_path" ]] && sh_path="$(pwd)"
	sh_path=$(realpath "$sh_path")
	while [[ -n "$sh_path" ]]
	do
		mounted_disks=$(lsblk -nlo name,mountpoint|grep /)
		d=$(grep -m1 "$sh_path" <<<"$mounted_disks" |cut -d" " -f1 2>/dev/null)
		[[ -z "$d" ]] && sh_path="${sh_path%/*}" || break
	done
	[[ "${d:0:2}" = "sd" ]] && d="${d%%[0-9]*}" || {
		[[ "${d:0:2}" = "nv" ]] && d="${d%p[0-9]*}"
	}
	echo "$d"
}

# Used if -p flag not used.
function guess_loader_part(){
local disk vfats_on_disk parts_available
	disk="$1"
	vfats_on_disk=$(lsblk -nlo name,partlabel,fstype|grep vfat|grep "$disk")
    parts_available=$(wc -l <<<"$vfats_on_disk")
	if [[ "$parts_available" -gt 1 ]]
	then
	    echo Warning. There are more than one vfat partitions on "$disk" >&2
	    echo -e "$vfats_on_disk" >&2
	    echo The first one will be used. >&2
	fi
	disk=$(head -1 <<<"$vfats_on_disk"|cut -d" " -f1)
	echo "${disk##*[^0-9]}"
}

# Used if -l flag not used.
function guess_loader(){
local vendor
	vendor="$(cat /sys/devices/virtual/dmi/id/board_vendor)"
	[[ "$vendor" == 'LENOVO' ]] &&
		echo vmlinuz-linux.efi ||
		echo vmlinuz-linux
}

# Used if -u flag not used/
function guess_micro-code(){
local cpu=''
	cpu=$(cat /proc/cpuinfo | grep -m1 -Eoi intel\|amd)
	cpu="${cpu,,}-ucode.img"
	[[ -f "/boot/$cpu" ]] && echo "${cpu}" || echo ''
}

function suggest_efi-entry_label(){
local -a labels
local dskl="$1" i=0 L
	L="$(lsblk -nlpo partlabel,fstype,name|grep "$dskl"|grep vfat|cut -d" " -f1)"
	labels=($(efibootmgr -u |awk '/initrd=/ {print $2}'))
	# Check if label exists and modify the esuggested label if necessary
	while printf '%s\0' "${labels[@]}" | grep -F -x -z -q -- "$L"
	do
		L="${L%.*}.$((++i))"
	done;
	echo "$L"
}

function guess_root_partlabel(){
local dskl="$1"
	echo $(lsblk -o partlabel,label,name|grep "$dskl"|grep -i root|cut -d" " -f1)
}

function set_efibootmgr_parameters(){
	[[ -z "$l_disk" ]] && l_disk="$(guess_loader_disk)"
	[[ -z "$l_part" ]] && l_part="$(guess_loader_part "$l_disk")"
	[[ -z "$loader" ]] && loader="$(guess_loader)"
	[[ -z "$eLabel" ]] && eLabel="$(suggest_efi-entry_label "$l_disk")"
	[[ -z "$r_disk" ]] && r_disk="$(guess_root_partlabel "$l_disk")"
	[[ -z "$ucode" ]] && ucode=$(guess_micro-code)
	# ((fallbk)) 	# Default: 0 → see function ramdisk_img
	# quiet='quiet' # quiet boot mode
	# extra_opts='' # not set up here
}

function usage(){
set_efibootmgr_parameters
echo -e "\nRun:\n\t${0##*/} [OPTIONS]\n"
cat << OPTS
OPTIONS
All are optional and guessed if not delcared explicitely.
-d loader_disk            : [/dev/]$l_disk $(printf "%$((${#loader}-${#l_disk}-8))s" '') # sda, sdb, nvme0n3
-p loader_boot_part_no    : $l_part $(printf "%$((${#loader}-${#l_part}-1))s" '') # 1, 2,...
-l loader                 : $loader # loader[.efi])
-L "efi-boot entry label" : $eLabel
-r root-partition_label   : $r_disk # PARTLABEL
-u <intel/amd>-ucode.img  : $ucode
-e "extra opts"           # extra kernel boot options
-f                        : $fallbk      # Use the fallback.img?
-Q                        : $quiet  # Don't boot quietly.
-P                        : $Profile      # create profile file, efi.profile_host
-N                        : $NSH_script      # create .nsh script, startup_host.nsh
-A                        : $AddEntry      # Add EFI entry to computer's NVRAM
-------
Extra kernel options:  $extra_opts

If none of -P/-N/-A is is used, nothing is set or created.
OPTS
}

function ramdisk_img(){
	((${1:-0})) &&
		echo initrd=initramfs-linux-fallback.img ||
		echo initrd=initramfs-linux.img
}

function modified_efibootmbg-v(){
local x y
	while read line
	do
		x=$(echo $line |sed 's/\(.*)\)\(.*\)/\1/g') # part where no modification is made
		y=$(echo $line |sed -e 's/\.\.\./ß/g' -e 's/\(.*)\)\(.*\)/\2/g;{s/\.//g}' -e 's/ß/./g' -e '/root/!d' )
		echo ${x}${y}
	done <<< $(efibootmgr -v)
}


while getopts "d:p:l:L:r:u:e:fQPNAhV" flag
do
    case "$flag" in
    	d) l_disk="$OPTARG"; l_disk=${l_disk#/dev/};; # loader disk
	p) l_part="$OPTARG";;     # loader-disk partition number
	l) loader="$OPTARG";;     # boot loader, loading the system
	L) eLabel="$OPTARG";;     # EFI entry label
	r) r_disk="$OPTARG";;     # root partition PARTLABEL
	u) ucode="$OPTARG";;      # µ-code option (intel, amd)
	e) extra_opts="$OPTARG";; # extra kernel boot options
	f) fallbk=1;; # use initramfs-linux-fallback.img rather than initramfs-linux.img
	Q) quiet='';; # boot verbously
#    	v) verb="--verbose";;
    	P) Profile=1;;
    	N) NSH_script=1;;
    	A) AddEntry=1;;
	h) abortf usage;;
	V) abortf "${0##*/} version ${VERSION}";;
    esac
done
# Remove the options parsed above.
#shift $((OPTIND - 1))
set_efibootmgr_parameters
((Profile+NSH_script+AddEntry)) || abortf usage

l_disk="/dev/$l_disk"
conc=''
[[ "${l_disk:5:4}" == 'nvme' ]] && conc='p'
l_diskpart=${l_disk#/dev/}${conc}${l_part}

if [[ -n "$ucode" ]]
then
	mpoint=$(lsblk -lo mountpoint,name|grep $l_diskpart|cut -d" " -f1)
	if [[ -z "$mpoint" ]]
	then
		echo -e "\nPartition ${l_diskpart} is not mounted."
		echo Cannot check if ${ucode} is already there.
	else
		[[ -f ${mpoint}/$ucode ]] || echo cp /boot/$ucode ${mpoint}/
		# Assumption:  EfiSystemPartition=/boot
	fi
	ucode="initrd=$ucode"
fi

# Make sure you do not use spaces in partlabels
# 'sgdisk -c ...' safely changes partlabel if needed

loader_line="root=PARTLABEL=${r_disk// /\\x20} rw $ucode $(ramdisk_img $fallbk)"
loader_line=$(echo $loader_line $extra_opts $quiet)
# The above removes leading space and double spaces
# (Don't use -e cause it reverses \x20 back into regular spaces)
efibootmgr_command="efibootmgr -d $l_disk -p $l_part --create -L \"$eLabel\" -l $loader -u \"${loader_line}\" --verbose"

name_fmt="$(hostnamectl hostname)_${l_disk#/dev/}${conc}${l_part}"

((Profile)) && {
	echo -e "\nPROFILE efi.profile.${name_fmt}"
	echo "#!/bin/bash"
	echo "${efibootmgr_command}"
}
((NSH_script)) && {
	echo -e "\nNSH SCRIPT startup.${name_fmt}.nsh"
	echo "$loader ${loader_line}"
}

if are_you_sure
then
#	echo "Yes, do."
	((NSH_script)) &&
	echo "$loader ${loader_line}" > startup.${name_fmt}.nsh &&
		echo Created file: startup.${name_fmt}.nsh

	((Profile)) &&
	echo "#!/bin/bash" > efi.profile.${name_fmt}
	echo "${efibootmgr_command}" >> efi.profile.${name_fmt} &&
		echo Created file: efi.profile.${name_fmt}

	SUDO=''
	(($EUID)) && SUDO='sudo'
	((AddEntry)) && {
		tmpf=$(mktemp)
		echo "${efibootmgr_command}" #|tee "$tmpf"
		$SUDO bash <<<${efibootmgr_command}
		#cat  <<<${efibootmgr_command}
		#&& modified_efibootmbg-v
	}
fi


#modified_efibootmbg-v
#lsblk -lo partlabel,partuuid,name,label,size,fstype|grep -E NAME\|vfat
