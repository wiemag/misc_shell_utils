#!/bin/bash
# by wm 2022-09-11
# Dependencies:
#  coreutils(realpath (or readlink -f))

version='1.0'
locbin='/usr/local/bin'

abortf(){
  [ $(type -t "$1")"" == 'function' ] && "$1" || echo -e "$1"
  exit "${2-0}"
}

usage(){
  echo -e "\nInstalls bash scripts in \e[1m${locbin}\e[0m by placing simlinks there."
  echo -e "Run:\n\t${0##*/} [-r] script_names.sh directories\n"
  echo -e "-r :  actually \e[1minstalls\e[0m the scripts; Otherwise it's a dry run.\n"
  echo If a script is already intalled, no attempt is made to re-install it.
  echo In case of directories, all '*.sh' scripts are installed recursively.
  echo -e "\nThe result or the 'would-be' result is printed for individual scripts."
}

failed_msg(){
local cmmt=${2- already installed.}
 echo -e '[\e[1;31mFailed\e[0m]: \e[1m'${1##*/}"\e[0m ${cmmt}"
}

result(){
local res=$?; cmmt=${2-}
 ((res)) && failed_msg "$1" "$cmmt" || echo -e '[\e[1;32mOK\e[0m]:     '$1
}

inst_script(){
local scpt="$1"
  if [[ "$scpt" =~ \  ]]
  then
    failed_msg "$scpt" " Skipping... File name has a space."
  else
    if [[ -e "${locbin}/${scpt##*/}" ]]
    then
      failed_msg "$scpt"
    else
      if (("$DRY"))
      then
        echo -e "[\e[33;1mDRY\e[0m]:    install \e[1m${scpt}\e[0m"
      else
        sudo ln -s -t "$locbin" $(realpath "$scpt") 2>/dev/null;
        result "$scpt" "";
      fi
    fi
  fi
}

[[ "${1-}" == '-r' ]] && { DRY=0; shift;} || DRY=1 
(($#)) || abortf "\n${0##*/}, v${version}\nMissing input.\nUse --help or -h." 1
[[ "${1-}" == '--help' || "${1-}" == '-h' ]] && abortf usage
echo

for f in "$@"	# only '*.sh' or a directory names are further accepted
do
  [[ "$f" == '.' || "$f" == '..' ]] && f="$(realpath '..')"

  [[ -f "$f" ]] && {
    [[ "$f" == *.sh ]] && inst_script "$f"  || failed_msg "$f" 'is not an *.sh'
  }
  [[ -d "$f" ]] && 
    while read script
    do
      inst_script "$script"
    done < <(find "$f" -type f -name '*.sh' -exec realpath {} \; )
done
if (("$DRY"))
then
  echo -e "\n\e[33;1mIt was a 'dry' run. Nothing has been installed.\e[0m"
  echo -e "Use the '\e[33;1m-r\e[0m' option to install the scripts actually."
  echo -e "If you see some '\e[31;1mFailed\e[0m' comments, those scripts won't be installed anyhow."
fi
