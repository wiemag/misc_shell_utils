#!/bin/sh
version='version 0.95'
[[ "$1" == '-k' ]] && KILL=1 || KILL=0

NOi3lock=0
scrshot_util=''

function install_check(){
hash scrot 2>/dev/null && scrshot_util='scrot' || {
	hash gnome-screenshot 2>/dev/null && scrshot_util=gnome-screenshot ||
	{ echo ${0##*/}: No screenhot utility installed; exit 10;}
}
hash feh 2>/dev/null || { echo ${0##*/}: Missing dependecy \'feh\'; exit 10;}
hash i3lock 2>/dev/null || { NOi3lock=1; hash slock 2>/dev/null;} || { echo ${0##*/}: Neither i3lock nor slock are installed; exit 10;};
return $NOi3lock;
}

reset_and_cleanup() {
  xset dpms 600 600 600;
  #echo DEBUG:  \$\#=$#
  [[ -z ${1-} ]] && xinput enable $TID;
  [[ $NOi3lock -eq 1 ]] && rm $scr2;
}

trap reset_and_cleanup HUP INT TERM

# Touchpad device id
TID=$(xinput list | sed -n '/Touchpad/ s/^.*=\([0-9]\+\).*$/\1/p')
#echo DEBUG: Touchpad ID = \$TID = $TID

# allow only one instance
r=$(pidof -x -o $$ ${0##*/})
set -- $r
if [ "${#r}" -gt 0 ]; then
	[[ $KILL -eq 1 ]] && { echo killing $r;
	xinput enable $TID;
	kill -9 $r;}
    exit
fi

install_check && {
	scr1=/tmp/${USER}_lckscr1.png; scr2=/tmp/${USER}_lckscr.png
	hash scrot 2>/dev/null && scrot -q 5 ${scr1} || gnome-screenshot -B -f ${scr1}
	convert $scr1 -scale 10% -scale 1000% -blur 4x4 -format png $scr2
	rm $scr1
}

xset +dpms dpms force off # Switch screen off immediately
xset +dpms dpms 4 4 4 # Change switching off to 4 seconds

xinput disable $TID   # Switch off touchpad
hash i3lock 2>/dev/null && i3lock -n -e -i $scr2 || slock
#reset_and_cleanup do_not_enable_touchpad # Could be necessary if i3lock were to be forked (? - not sure)
reset_and_cleanup # enable touchpad
