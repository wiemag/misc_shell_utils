#!/bin/bash

VERSION='0.02'
pw='' # password (the same for all the input files)

arg1="$1"

[[ $# -eq 0 || "$arg1" = '-h' || "$arg1" = '--help' ]] && {
	echo -e "\nUsage info for version ${VERSION}."
	echo "Decrypts a series of .7z files."
	echo -e "One password for all the files.\nRun:";
	echo -e "\t${0##*/} file1.7z [file2.7z [file3.7z [...]]]";
	exit 1;
}

[[ "$arg1" =~ ^-p ]] && {
	shift
	if [ "${#arg1}" -eq 2 ]
	then
		pw="$2"
		shift
	else
		pw="${arg1:2}"
	fi
}

[[ "$@" =~ '*' || "$@" =~ '?' ]] && { echo No input files.; exit 2;}

[[ -z "$pw" ]] && read -p "Input the password " -s pw
echo

for f in "$@"; do
	echo Extracting $f
	7z t "$f" -p fake_password >/dev/null 2>&1 && {
		echo $f has likely no password! Trying without password.
		7z e "$f" > /dev/null || { echo No password does not work for $f; exit 3;}
	} || {
		7z e "$f" -p"$pw" >/dev/null || { echo Error while decrypting $f; exit 1;}
	}
done
