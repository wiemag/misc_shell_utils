#!/bin/bash
# http://askubuntu.com/questions/69556/how-to-check-battery-status-using-terminal
#upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep --color=never -E "state|to full|to empty|percentage"
upower -i $(upower -e | grep BAT) | grep --color=never -E "state|energy-rate|time to|percentage"

#upower -i $(upower -e | grep Display) | grep --color=never -E "state|energy-rate|to full|percentage|to empty"
