#!/bin/bash

opt=${1-}
[[ "$opt" == '--help' || "$opt" == '-h' ]] && {
	echo Remove links and unused German and English dictionaries;
	echo from /usr/share/hunspell/;
	echo so they are not listed in thunderbird;
	exit
}

[[ "$EUID" -eq 0 ]] || SUDO='sudo'

LANG=C sudo -nv  2>&1|grep -qo ^Sorry && {
	echo You have not got the required premissions.
	exit 1;
}

# option -r:  do not run xargs if no arguments
find /usr/share/hunspell/ -type l -name '*en_*' -o -name '*de_[ABCL]*' |"$SUDO" xargs -r rm
find /usr/share/hunspell/ -name '*en_AU*' -o -name '*en_CA*'|"$SUDO" xargs -r rm
find /usr/share/hunspell/ -name '*de_[ABCL]*'|"$SUDO" xargs -r rm
