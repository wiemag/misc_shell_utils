#!/usr/bin/bash
version='0.02'
force=''
SCRIPT="${1:-}"
[[ "$SCRIPT" == '-f' || "$SCRIPT" == '--force' ]] && {
	force='--force' ; SCRIPT="${2:-}"
}
[[ -z "$SCRIPT" ]] && { echo Missing script/file name to be installed.; exit 1;}
(($EUID)) && { echo Please run ${0##*/} with root privilages.; exit 2;}
LANG=C sudo ln -s $force "$(realpath $SCRIPT)" /usr/local/bin/ || \
	echo -e 'No success.\nConsider using the --force|-f option.'
