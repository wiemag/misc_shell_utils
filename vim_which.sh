#!/bin/bash
# Edit only the first file in the command line.
#
[ -x $TERM ] || TERM=$(which sakura)

function writeperm(){
local perms=$1 who=$2
	case $who in
	"U") grep -q w <<<${perms:1:3};; # true if user has write permission
	"G") grep -q w <<<${perms:4:3};; # true if group has write permission
	"O") grep -q w <<<${perms:7:3};;
	esac
}
function user_belongs_to_fgroup(){
local u=$1 gr=$2
	grep -q \(${gr}\) <(id ${u})
}

(($#)) || { echo Missing input.; exit 1;}
input=$(readlink -e $(which "$1" 2>/dev/null) 2>/dev/null)
[[ -z "$input" ]] && { echo Wrong input file name.; exit 2;}
# -L dereference links not needed because of the use of 'readlink'
read fowner fgroup fperms < <(stat --format "%U %G %A" $(which $input))
#[[ -O "$0" ]] &&
#	echo "script belongs to user running it" ||
#	echo "script is not owned by user running it"
if [[ $USER == $fowner ]] && writeperm $fperms 'U'
then
	echo EDIT AS OWNER
	$TERM -e $EDITOR "$input" &
elif user_belongs_to_fgroup $USER $fgroup && writeperm $fperms 'G'
then
	echo EDIT AS GROUP MEMBER
	$TERM -e $EDITOR "$input" &
elif writeperm $fperms 'O'
then
	echo EDIT AS OTHERS
	$TERM -e $EDITOR "$input" &
else
	echo input=$input
	echo File owner/group/perms: $fowner $fgroup $fperms
	echo "                  "User: $USER
	echo "Can't edit -- no permissions."
	$TERM -e less "$input" &
fi
