#!/bin/bash
# Input files grouped into batches and 7-zipped.
# Default batch size set to 4. Use -b to change.
# Filter (-f) MUST BE SET explicitly in the command line,
# using (single-)quote marks if necessary.

source bashf.sh || { echo bashf.sh needed; exit 1;}
show_version() { echo "${0##*/} 1.01 (2020-05-31)"; exit; }
filter="" # regular expression filter
declare -i NoF # number of files
declare -i bat_size=4 # default
declare -i N=0 # number of batches
declare -i i j # indeces
archive="${PWD##*/}" # archive name, default is the current folder name
password="" # password for all batches

ARG_PARSE_OPTS+=(
	f filter --desc="RegEx filter (as used by find) in place of a regular one" =:val
	b bat_size --desc='No of files per batch (default: 4)' =:val
	n archive --desc="Archive name (default [folder.7z]: '${PWD##*/}')" =:val
	p password --desc='One password for all batches' =:val
    v version --desc='Print script version no and exit' '{ show_version; }'
)
ARG_PARSE_REST=(--opt-var=files --opt-min=0)
arg_parse "$@"

# If -f used any file names paresed and put in ${files[@]} from command line are igneored.
if [ -n "$filter" ]
then
	files=() # zero out whatever may have been in the $files
	mapfile -d $'\0' files < <(find . -maxdepth 1 -not -path '*/\.*' -type f -regex "$filter" -print0|sort -z|sort -z)
fi
has_val files || die_return 2 "No input files" # or opt-min=1

zip_opts=()
[ -n "$password" ] || read -p "Input the password: " -s password
[ -z "$password" ] && echo Password not set. || zip_opts+=(-p"$password" -mhe)
NoF=${#files[@]}
printf "Number of files to compress: %d\n" $NoF

N=$(( $NoF % $bat_size ? 1 : 0 ))
N=$((N + $NoF / $bat_size)) # Number of 7-zipped batches

curr_no=0
for ((i=0; i < $N; i++));
do
	batch=()
	bsize=$(( NoF - curr_no > bat_size ? bat_size : NoF - curr_no));
	for ((j=0; j<bsize; j++))
	do
		batch+=("${files[curr_no+j]#./}")
		echo "${files[curr_no+j]#./}"
	done
	new_archive="${archive}_$(printf "%0${#N}d" $((i+1))).7z"
	7z a ${zip_opts[@]} "$new_archive" "${batch[@]}" 2>&1 >/dev/null && \
		7z t -p"$password" "$new_archive" 2>&1 >/dev/null && \
			printf "File '%s' created. Integrity checked OK.\n" "$new_archive"
	curr_no=$((curr_no+bsize))
done
echo
if [[ $N -gt 1 ]]
then
	log_info $N archives have been created.
	log_info ${archive}_$(printf "%0${#N}d" 1).7z .. ${archive}_$(printf "%d" $N).7z
else
	log_info One archive has been created:  ${archive}_$N.7z
fi
