#!/bin/bash
version=0.01

# returns 0 if PESEL is mathematically correct
pesel_mathchk() {
  local debug=$(($#>1))  # debug is only printed if errors are found
  declare -i w=0 pesel="${1:-}"
  ((${#pesel} == 11)) || {
    ((debug)) && >&2 echo Wrong PESEL number.;
    exit 1;
  }
  w=$((${pesel:0:1}+${pesel:4:1}+${pesel:8:1}+3*(${pesel:1:1}+${pesel:5:1}+${pesel:9:1})+7*(${pesel:2:1}+${pesel:6:1})+9*(${pesel:3:1}+${pesel:7:1})));
  w=$((10-w%10));
  ((${pesel:10:1}==$w)) || {
    ((debug)) && !  { >&2 echo Last digit of pesel should be $w.;}
  }
}

# retuns 0 if the birthdate encloded in the PESEL is a valid date
# and outputs such a date in the YYYY-MM-DD format
pesel_birthdate() {
  local prnt=$(($#>1))  # print date if correct and if prnt>0
  declare -i yr mo dy pesel="${1:-}"
  local bdate
  mo=${pesel:2:2}; # month + encoded century
  yr=$((mo/20));  # 0=1900, 1=2000, 2=2100, 3=2200, 4=1800
  ((yr>3)) && yr=-1
  yr=$((1900+100*yr+${pesel:0:2})) # year: YYYY {1900..2299}
  mo=$((mo%20));  # month
  dy=${pesel:4:2}
  bdate=$(date '+%F' -d "${yr}-${mo}-${dy}" 2>/dev/null) # Format: YYYY-MM-DD
  [[ -n "$bdate" ]] && { ((prnt)) && echo $bdate || : ;}
}

# returns 0 is the pesel is correct
# returns 1 if the script it is mathematicaally incorrect
#
chk_pesel() {
  local pesel="${1:-}" sex="${2:-}" # valid sex = m/M/1 or f/F/0/2
  pesel_mathchk "$pesel" || exit 1 # mathematically incorrect
  pesel_birthdate "$pesel" || exit 2 # wrong birth date
  [[ -z "$sex" ]] || {
    case "${sex,,}" in
      'f'|0|2) sex=0;;
      'm'|1) sex=1;;
    esac
    (( $(("${pesel:9:1}" % 2)) == "$sex")) || exit 3 # wrong sex
  }
}

chk_pesel "$@"
