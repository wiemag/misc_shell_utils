#!/bin/bash
# https://wiki.archlinux.org/title/Securely_wipe_disk/Tips_and_tricks
# dependency: openssl, util-linux (lsblk)
hash openssl > /dev/null || {
	echo "Missing dependency:  openssl";
	exit 1;
}

echo -e "\nBlock devices to choose from:"
lsblk -ndo name,size
echo
read -p "Which block device to mount? "
DEVICE="$REPLY"
[[ -n "$DEVICE" || ${#DEVICE} -eq 1 ]] || { echo "No device chosen."; exit 0;}

No_of_devices=$(grep "$DEVICE" <(lsblk -nd -o name)|wc -l)

[[ $No_of_devices -gt 0 ]] || { echo "No such block device."; exit 2;}
[[ $No_of_devices -eq 1 ]] || {
	echo "Ambiguous... ";
	grep --color=auto "$DEVICE" <(lsblk -nd -o name)
	exit 2;
}

DEVICE=/dev/$(grep --color=auto -E "${DEVICE}[^ ]*" <(lsblk -nd -o name))

mount|grep -q "$DEVICE" && {
	echo -e "\nDevice or partition '$DEVICE' is mounted. Aborting...";
	exit 3;
}

echo -ne "Are you sure it is \e[5m\e[1;33m${DEVICE}\e[0m?  (n/y) "
read -n1
[[ -z $REPLY ]] || echo
[[ ${REPLY,,} != 'y' ]] && { echo Aborting...; exit;}

PASS=$(tr -cd '[:alnum:]' < /dev/urandom | head -c128)
sudo openssl enc -aes-256-ctr -pass pass:"$PASS" -nosalt </dev/zero | dd obs=64K ibs=4K of=$DEVICE oflag=direct status=progress
