#!/bin/bash
version=0.99
# Available USB devices
avaUSB=$(ls -l /dev/disk/by-id/ |awk '/usb|USB/{if(!/[0-9]$/){print $NF}}'|sed 's/.*\//\/dev\//')

DEV="${1:-}"
[[ "$DEV" == -v ]] && { echo -e "\n  ${0##*/}, version ${version}"; exit;}
[[ -z "$DEV" || "$DEV" == '-h' || "$DEV" == '--help'  ]] && {
	echo -e "\nRun:\n\t${0##*/} usb_device\n";
	[[ -z "$DEV" ]] && echo -e "Missing parameter:  usb_device.\n" ||
		echo -e "Wiping file system info (wipefs -a) and zeroing the first 64 MB.\n";
	echo Available USB devices.;
	lsblk -nlpo name,size,fstype,label,partlabel,mountpoint ${avaUSB};
#	lsblk -nplo name,size,fstype,label,partlabel,mountpoint;
	exit 1;
}
DEV="$(lsblk -nlpo name |grep -Ev [0-9]$ |grep "$DEV")"
[[ -z "$DEV" ]] && {
	echo -e "\nWrong device name. Not able to identify the USB device.";
	exit 2;
}
echo -ne "About to run\n\tsudo wipefs -a $DEV\nAre you sure (y/N)"
ANSW='N'
read -rsn 1 ANSW && [[ -z "$ANSW" ]] && echo
ANSW=${ANSW^^}
echo -e "\n${#ANSW} $ANSW"
[[ "$ANSW" == "Y" ]] && echo -e "\nwipefs -a $DEV"
#exit 111
sudo wipefs -a "$DEV" &&
sudo dd bs=4M count=16 if=/dev/zero of="$DEV" status=progress &&
	echo echo 4MB x 16 has been wiped with zeroes, too.
