#!/bin/bash
version=0.03
debug=0
SRC=''	# source; set before rsync'ing
DST='' 	# destination path for rsync; set for individual $SRC paths
dst_partlabel='4sdc-DATA'	# destination ($DST) partition label
SSH=0   # save .ssh if 1
GPG=0   # save .gunpg if 1
err_no_disk=8     # Missing-backup-disk error code
err_ambiguous=9   # Provided partition label not specific enough.
flag='' # script option flag

# Deps:
# rsync util-linux grep coreutils udisks2
#
function abortf() {
	[ $(type -t "${2:-}")"" != 'function' ] && echo -e "$2" >&2 || "$2";
    exit "${1-0}";
}

usage() {
	echo -e "\nRun:\n\t${0##*/} [-s][-g] [PARTLABEL] | --help\n"
cat <<NFO
Backs up dot.file on a partition defined by PARTLABEL.
PARTLABEL defaults to $dst_partlabel

.ssh and .gnupg directories are not backed up without the below flags
-s  :  backs up the .ssh directory
-g  :  backs up the .gnupg directory
NFO
}


flag="${1:-}"
[[ "$flag" == '--version' ]] && abortf 0 "\n${0##*/}, version=$version"
[[ "$flag" == '--help' ]] && abortf 0 usage
[[ "$flag" == '--debug' ]] && { debug=1; shift;}

while getopts "vhdsg" flag
do
 case "$flag" in
  v) abortf 0 "\n${0##*/}, version=$version";;
  h) abortf 0 usage;;
  d) debug=1;;
  s) SSH=1;;
  g) GPG=1;;
 esac
done
# Remove the options parsed above.
shift $(expr $OPTIND - 1)
(($#)) && dst_partlabel="$1"  # The default value already defined.


dst_disk_mount(){
local lab="$1" # PARTLABEL
local dev path=''
	dev="$(lsblk -nlpo name,partlabel |grep "$lab"|cut -d" " -f1)"
	if [[ -z "$dev" ]]
	then
		# The abortf does not terminate the script, because this function
		# is supposed to be run in a sub-shell:  variable=$(sub-shell).
		# Instead, it prints the message and acts like 'return'.
		abortf "$err_no_disk" "\nPartition (PARTLABEL) '$lab' not found."
		echo THIS LINE WILL NOT BE PRINTED >&2
		exit 196 # THIS LINE WILL NOT BE EXECUTED
	fi
	if (($(wc -l <<<$dev)-1))
	then
		# The abortf does not terminate the script, because this function
		# is supposed to be run in a sub-shell:  variable=$(sub-shell).
		abortf "$err_ambiguous" "\nMore than one PARTLABELs '$lab' found."
	fi
	path="$(mount | grep "$dev" | cut -d" " -f3)" 	# Is $dev mounted? Where?
	if [[ -n "$path" ]]
	then
		if ! grep -q "/media/${USER}/" <<<"${path}"
		then
			>&2 echo -e "\nPartition '$lab' likely mounted by another user"
			>&2 echo "at ${path}."
			>&2 echo "Have it unmounted."
			return 7 #   See commnets at the abortf above.
		fi
	else
		path="$(udisksctl mount -b "$dev" | cut -d" " -f4)"
	fi
	echo "$path"
}


# Backing up the public folder (/home/cache)

# Backing up the dot.files/dot.folders
SRC="${HOME}"
DST="$(dst_disk_mount "$dst_partlabel")"
(($?>7)) && abortf "$err_no_disk" "Plug the external drive in."
if [[ -z "$DST" ]]
then
	echo Skipping...
else
	srcs0=(.bash{rc,_profile} .xinitrc .Xresources .vimrc .vim .local)
	srcs1=(.config/{alacritty,fontconfig,libreoffice,mpv,mysettings,nemo,openbox,rofi,tint2,volumeicon,sakura,mimeapps.list})

	# Create dest. directory for the machine if the directory doesn't exist
	# Do it in steps because setting permisions/groups is not recursive
	# with 'mkdir -m... -p' or with 'install -d -m... -g...'.
	DST="${DST}/machines"
	[[ -d "$DST" ]] || install -d -g 1000 -m 775 "$DST" ||
		sudo chown "${USER}":1000 "$DST" ||
			abortf 11 "\nProblem with permisions/groups"
	DST="${DST}/$(hostnamectl hostname)"
	[[ -d "$DST" ]] || install -d -g 1000 -m 775 "$DST" ||
		sudo chown "${USER}":1000 "$DST" ||
			abortf 12 "\nProblem with permisions/groups"

	# Create destination directory for a user on the machine.
	DST="${DST}/${USER}/dotfiles/"
	[[ -d "$DST" ]] || mkdir -p "$DST"

	# Go to the source directory
	1>/dev/null pushd "${SRC}"
	echo -e "\nBacking up dot.files"
	for f in "${srcs0[@]}" "${srcs1[@]}";
	do
		if [[ -e "$f" ]]
		then
			echo $f >&2
			DST_f="${DST}${f%${f##*/}}"
			#echo 'rsync [-n] --delete -aur -OJP' "$f" "${DST_f}"
			rsync -q --delete -aur -OJP "$f" "${DST_f}"
		else
			echo Non-existent: ${f}. Skipping...
		fi
	done
	popd >/dev/null

	echo -e "\nToDo list:"
	echo • copy useful scripts to usb/public
	echo • consider some etc/configurations
	echo • consider some systemd services
	echo • consider some pacman hooks
	echo • consider another definition of what to back-up + what to skip
	echo • DO NOT BACK UP .ssh unless specifiaclly askes for, flag -s
	echo • DO NOT BACK UP on the partition where the dot.files are located
fi
